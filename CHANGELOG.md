# Changelog

#### 1.0.3

- Use mTask server 5

#### 1.0.2

- Use newest mTask version
- Update devcontainer

#### 1.0.1

- Use newest mTask version

## 1.0.0

- Initial version
