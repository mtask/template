module App

import mTask.Interpret
import mTask.Interpret.Device.TCP
import iTasks

Start w = doTasks main w

main :: Task ()
main = updateInformation [] {TCPSettings|host="192.168.1.1", port=Port 8123, pingTimeout= ?None}
	<<@ Title "Device details"
	>>? \dd->withDevice dd \dev->
		liftmTask blinkTask dev
	>>* [OnAction (Action "Stop") (always (return ()))]
where
	blinkTask :: Main (MTask v Bool) | mtask v
	blinkTask = declarePin D13 PMOutput \ledPin->
		fun \blink=(\st->
			     writeD ledPin st
			>>|. delay (lit 500)
			>>|. blink (Not st)
		) In {main=blink true}
