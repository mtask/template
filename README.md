# mTask template

This is a template application that you can use as the basis for your mTask
applications.
It is based on the iTask version: https://gitlab.com/clean-and-itasks/itasks-template

It contains support for devcontainers.

## How to run

```bash
nitrile fetch
nitrile build
./bin/App
```

See the main repository for information on how to run the client.

## License

The project is licensed under the BSD-2-Clause license.
